# NowScreenConfig

This is the publicly available configuration for NowScreen devices.  In this config we:

    * Install our apt source,
    * Install our applications nowscreen-boot and python-nowscreen-nodemanager from the apt source,
    * Ensure this config is applied weekly (and thus any updates),
    * Apply various system-level tweaks.

The configuration is applied using Ansible.  Someone proficient in Ansible should find the playbooks
straightforward to read.
