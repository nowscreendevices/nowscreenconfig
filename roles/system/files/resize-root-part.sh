#!/bin/bash

get_part_size() {
    DEV=$1

    echo $(lsblk --output SIZE --nodeps --bytes --noheadings "/dev/$DEV")
}

if ! [ -b /dev/mmcblk0 ]; then
    echo "Don't have /dev/mmcblk0: probably not running on rpi"
    exit 1
fi

if grep 'init=/usr/lib/raspi-config/init_resize.sh' /boot/cmdline.txt; then
    echo "Already set to resize on next reboot"
    exit 1
fi

ROOT_PART_NAME=$(findmnt / -o source -n | cut -d"/" -f3)
ROOT_PART_DEV=$(echo /sys/block/*/"$ROOT_PART_NAME" | cut -d"/" -f4)

BOOT_PART_NAME=$(findmnt /boot -o source -n | cut -d"/" -f3)
BOOT_PART_DEV=$(echo /sys/block/*/"$BOOT_PART_NAME" | cut -d"/" -f4)

if [ "$ROOT_PART_DEV" != "$BOOT_PART_DEV" ]; then
    echo "/ and /boot are on different devices."
    exit 1
fi

DISK_SIZE=$(get_part_size "$ROOT_PART_DEV")
BOOT_SIZE=$(get_part_size "$BOOT_PART_NAME")
ROOT_SIZE=$(get_part_size "$ROOT_PART_NAME")

DISK_EMPTY=$(($DISK_SIZE - $BOOT_SIZE - $ROOT_SIZE))

THRESHOLD=$((50 * 1024 * 1024)) # 50 MB
if [ "$DISK_EMPTY" -gt "$THRESHOLD" ]; then
    echo 'Resizing on next reboot'
    sed -i 's|$| init=/usr/lib/raspi-config/init_resize.sh|' /boot/cmdline.txt
else
    echo 'SD card (almost) full - nothing to do'
fi

exit 0
